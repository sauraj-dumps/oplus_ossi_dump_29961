## sys_mssi_64_cn_armv82-user 13 TP1A.220905.001 1692076175476 release-keys
- Manufacturer: oplus
- Platform: common
- Codename: ossi
- Brand: oplus
- Flavor: sys_mssi_64_cn_armv82-user
- Release Version: 13
- Kernel Version: 5.10.149
- Id: TP1A.220905.001
- Incremental: 1692076175476
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oplus/ossi/ossi:12/SP1A.210812.016/1692081404249:user/release-keys
- OTA version: 
- Branch: sys_mssi_64_cn_armv82-user-13-TP1A.220905.001-1692076175476-release-keys
- Repo: oplus_ossi_dump_29961
